package ist.challenge.rizki_fadilah.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ist.challenge.rizki_fadilah.model.USER;
import ist.challenge.rizki_fadilah.repository.USER_REPOSITORY;


@Service
public class USER_SERVICE {

    @Autowired
	private USER_REPOSITORY userRepo;

    public Integer findbyUsernameExisting(String username) {
		return this.userRepo.findbyUsernameExisting(username);
	}

    public Integer findbyPasswordExisting(String password) {
		return this.userRepo.findbyPasswordExisting(password);
	}

    public USER getDataExistUsername(String username) {
		return this.userRepo.getDataExistUsername(username);
	}

    public USER getDataExistPassword(String password) {
		return this.userRepo.getDataExistPassword(password);
	}

    public USER edit(Long id,USER item) {
		USER data = this.userRepo.findById(id).get();
		data.setUsername(item.getUsername());
		data.setPassword(item.getPassword());
		USER resp = this.userRepo.save(data);
		return resp;
	}

	public USER findUserId(Long id) {
		return this.userRepo.findUserId(id);
	}

	public List<USER> getListUsers() {
		return this.userRepo.getListUsers();
	}

	public String getDataUsername(String username) {
		return this.userRepo.getDataUsername(username);
	}

	public String getDataPassword(String password) {
		return this.userRepo.getDataPassword(password);
	}
}
