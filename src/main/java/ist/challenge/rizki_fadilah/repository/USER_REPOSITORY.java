package ist.challenge.rizki_fadilah.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ist.challenge.rizki_fadilah.model.USER;



@Repository
public interface USER_REPOSITORY extends JpaRepository<USER, Long>{ 

    @Query(value="SELECT COUNT(*) AS total FROM USERS WHERE USERNAME =:username",nativeQuery = true)
	Integer findbyUsernameExisting(@Param("username")String username);

    @Query(value="SELECT COUNT(*) AS total FROM USERS WHERE password =:password",nativeQuery = true)
	Integer findbyPasswordExisting(@Param("password")String password);

    @Query(value="SELECT * FROM USERS WHERE PASSWORD =:password",nativeQuery = true)
	USER getDataExistPassword(@Param("password")String password);

    @Query(value="SELECT * FROM USERS WHERE USERNAME =:username",nativeQuery = true)
	USER getDataExistUsername(@Param("username")String username);

    @Query(value="SELECT * FROM USERS WHERE ID =:id",nativeQuery = true)
	USER findUserId(@Param("id")Long id);

    @Query(value="SELECT * FROM USERS",nativeQuery = true)
	List<USER> getListUsers();

    @Query(value="SELECT USERNAME FROM USERS WHERE USERNAME =:username",nativeQuery = true)
	String getDataUsername(@Param("username")String username);

    @Query(value="SELECT PASSWORD FROM USERS WHERE PASSWORD =:password",nativeQuery = true)
	String getDataPassword(@Param("password")String password);

}