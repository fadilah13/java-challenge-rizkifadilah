package ist.challenge.rizki_fadilah.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.GenerationType;
import javax.persistence.Table;

@Entity
@Table(name="users")
@SequenceGenerator(name= "seq_users", sequenceName = "SEQ_ID_USERS", initialValue=1, allocationSize = 1)
public class USER {
    @Id
	@GeneratedValue(generator="seq_users",strategy=GenerationType.AUTO)
	private Long id;

	@Column(name = "username", length = 25, unique = true)
	private String username;

    @Column(name = "password", length = 25)
	private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

