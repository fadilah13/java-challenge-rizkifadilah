package ist.challenge.rizki_fadilah.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.security.sasl.AuthenticationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ist.challenge.rizki_fadilah.model.USER;
import ist.challenge.rizki_fadilah.repository.USER_REPOSITORY;
import ist.challenge.rizki_fadilah.service.USER_SERVICE;


@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
public class USER_CONTROLLER {
    String status = "status";
	String success = "Success";
	String failed = "Fail";

    @Autowired
	private USER_SERVICE userService;

    @Autowired
	private USER_REPOSITORY userRepo;

    @PostMapping(value="/login")
	public ResponseEntity<?> login(@RequestParam String username, String password) {
		Map<String, Object> body = new HashMap<>();
		String getUser = this.userService.getDataUsername(username);
		String getPass = this.userService.getDataPassword(password);
		try {
			if(getUser == null || getPass == null ) {
                body.put("message", "Username dan / atau password kosong”");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST); 
			} else if(getUser.equals(username) || getPass.equals(password)){
				body.put(status, HttpStatus.OK.value());
                body.put("message", "Sukses Login");
				return new ResponseEntity<>(body, HttpStatus.OK);
            } else {
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}

    @PostMapping(value="/register")
	private ResponseEntity<?> register(@RequestBody USER item){
		Map<String, Object> body = new HashMap<>();
		try {
			Integer findbyUsernameExisting = this.userService.findbyUsernameExisting(item.getUsername());
			if(findbyUsernameExisting>0) {
				body.put("message", "Username Sudah Terpakai");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
			} else {
                USER user = this.userRepo.save(item);
				body.put("status", "Success");
                body.put("message", "Berhasil Registrasi");
                body.put("data", user);
				return new ResponseEntity<>(body, HttpStatus.OK); 
			}
		} catch (Exception e) {
			e.printStackTrace();
			body.put("message", e.getMessage());
			return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
		}
	}

    @PutMapping(value="/update/{id}")
	private ResponseEntity<?> edit(@PathVariable("id")Long id,@RequestBody USER item){
		Map<String, Object> body = new HashMap<>();
		Integer findbyUsernameExisting = this.userService.findbyUsernameExisting(item.getUsername());
		USER dataFindbyUserId = this.userService.findUserId(id);
		USER data = this.userRepo.findById(id).get();
		try{
			if(dataFindbyUserId==null) {
				body.put(status, HttpStatus.BAD_REQUEST.value());
				body.put("message", "ID tidak ditemukan");
				return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
				}else if(findbyUsernameExisting>0){
						body.put("message", "Username Sudah Terpakai");
						return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
				}else{		
					if(item.getPassword().equals(data.getPassword())){
						body.put(status, failed );
						body.put("message", "Password tidak boleh sama dengan password sebelumnya");
						return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
				 	
						} else {
								USER dataSave = this.userService.edit(id, item);
								body.put(status, success);
								body.put("data", dataSave);
								return new ResponseEntity<>(body, HttpStatus.OK);
						}
					}
		} catch (Exception e) {
			body.put(status, failed);
			body.put("data", null);
			return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping(value="/users")
    private ResponseEntity<?> getUser(){
        Map<String, Object> body = new HashMap<>();
        List<USER> list_user = this.userService.getListUsers();
        if(list_user!=null) {
            body.put(status, HttpStatus.OK.value());
            body.put("message", success);
            body.put("data", list_user);
            return new ResponseEntity<>(body,HttpStatus.OK);
        }else {
            body.put(status, HttpStatus.BAD_REQUEST.value());
            body.put("message", "Data tidak ditemukan");
            return new ResponseEntity<>(body,HttpStatus.BAD_REQUEST);
        }
    }
}
