package ist.challenge.rizki_fadilah;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RizkiFadilahApplication {

	public static void main(String[] args) {
		SpringApplication.run(RizkiFadilahApplication.class, args);
	}

}
